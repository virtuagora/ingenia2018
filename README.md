# Welcome to Virtuagora 2.0 for Ingenia 2018

**WARNING: This repository is still under development for the version 2 of Virtuagora.**
If you need information or support please contact us! 
[@augusthur][2] & [@guillermocroppi][3]

Right now the project is *quite* unstable, but because it is still in development!
Want to know when it will be available? Follow us on Twitter  [@virtuagora][1]

---

### Installation
Open a terminal in the desire folder where you want to `git clone` the repository.
```bash
git clone https://github.com/virtuagora/virtuagora-core.git
```
Install the php dependencies using composer
```bash
php composer install
```
Install the npm dependencies
```bash
npm install
```

#### Changelog

##### v1.1.1

- Se agregó un countdown para que sepan como vienen con las fechas ;)
- Se arreglo un problema con ver la información de un equipo asociado a una organización.

##### v1.1
- ¡Ahora pueden bancar proyectos! En cada HUB del proyecto podrán encontrar el botón de bancar.
- Agregados los botones de compartir en Facebook, Twitter y Whatsapp (Solo funciona en móviles)
- Se agregó la posibilidad de dejar comentarios a los proyectos. Se pueden responder a cada comentario.
- Agregamos también la posibilidad de que puedan "Aplaudir" a los comentarios y respuestas.
- Nueva funcionalidad: Ahora los responsables pueden borrar el grupo. Si tienen ya un proyecto hecho, tambien se borrará. Todos los miembros del equipo tambien se desligan y pueden unirse a otros grupos de querer.

ARREGLOS y MEJORAS: 

- Ahora los responsables de los proyectos tendran un icono que les avisará si tienen una solicitud pendiente
- Arreglado el problema que no podian enviar una solicitud para colaborar en un proyecto.
- Arreglado un problema al subir fotos de DNI. Los que hayan tenido problemas, ¡Vuelvan a intentar!
- Ahora se ve correctamente en el HUB del proyecto, en el apartado del equipo, quien es el Responsable y el Co-responsable
- Por alguna razon "Verificar email" no estaba apareciendo correctamente para aquellos que se registraban con Facebook, (Si te registraste usando Facebook, probablemente para ser parte de un equipo te este faltando este paso... Si te encontraste con este problema, y si asi fue tu caso, te pedimos por favor que intentes verificar tu Email ahora!) 
- Agregada la sección de "Ayuda" donde podrán saber como contactarnos para mandar sugerencias o reportar un error con la plataforma.
- Arreglado otros bugs y mejoras visuales.

